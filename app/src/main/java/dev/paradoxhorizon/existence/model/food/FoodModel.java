package dev.paradoxhorizon.existence.model.food;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class FoodModel {
    private int uid;
    private String name;
    private double health;
    private double price;
    private double rating;
    private String location;
}
