package dev.paradoxhorizon.existence.model.filter;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class FilterFoodModel {
    private String name;
    private double minRating;
    private double maxRating;
    private double minHealth;
    private double maxHealth;
    private double minPrice;
    private double maxPrice;
}
