package dev.paradoxhorizon.existence.model.food;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;

public interface SupabaseFoodService {
    @GET("/rest/v1/foods")
    Call<List<FoodModel>> getAllFoods(@HeaderMap Map<String, String> headers);
}
