package dev.paradoxhorizon.existence.common.structure.constants;

public class FormMode {
    public static final int INSERT = 0;
    public static final int UPDATE = 1;
    public static final int DELETE = 2;
    public static final int READ = 3;
}
