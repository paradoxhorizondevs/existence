package dev.paradoxhorizon.existence.common.utils;

import java.util.Random;

public class NumberUtil {

    public static int generateRandomNumber(int lower, int upper) {
        // TODO: Check for alternatives. Based on initial testing, this
        //  function has heavy bias towards either a 1 or 2 out of 8 choices.
        //  It might be my luck, but still.
        Random random = new Random();
        return random.nextInt((upper - lower + 1)) + lower;
    }

    public static double parseDoubleSafely(String number) {
        if (number.isBlank()) {
            return 0;
        } else {
            return Double.parseDouble(number);
        }
    }
}
