package dev.paradoxhorizon.existence.common.structure.constants;

public class SourceScreen {
    public static final int HOME = 1;
    public static final int SEARCH = 2;
    public static final int DASHBOARD = 3;
    public static final int MODIFY_FOOD = 11;
}
