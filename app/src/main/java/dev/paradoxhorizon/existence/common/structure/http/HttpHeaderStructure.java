package dev.paradoxhorizon.existence.common.structure.http;

public class HttpHeaderStructure {
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String KEY_BEARER = "Bearer ";
}
