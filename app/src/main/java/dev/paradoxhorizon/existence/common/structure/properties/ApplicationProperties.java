package dev.paradoxhorizon.existence.common.structure.properties;

public class ApplicationProperties {
    public static final String FILE_NAME = "application.properties";
    public static final String SUPABASE_HOST = "supabase.host";
    public static final String SUPABASE_APIKEY = "supabase.apikey";
}
