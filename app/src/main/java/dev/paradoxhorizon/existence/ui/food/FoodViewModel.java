package dev.paradoxhorizon.existence.ui.food;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import dev.paradoxhorizon.existence.model.food.FoodModel;

public class FoodViewModel extends ViewModel {
    private final MutableLiveData<FoodModel> mFoodModel;

    public FoodViewModel() {
        this.mFoodModel = new MutableLiveData<>();
    }

    public LiveData<FoodModel> getFoodModel() {
        return mFoodModel;
    }

    public void setFoodModel(FoodModel model) {
        this.mFoodModel.setValue(model);
    }
}
