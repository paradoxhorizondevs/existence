package dev.paradoxhorizon.existence.ui.food;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;

import dev.paradoxhorizon.existence.R;
import dev.paradoxhorizon.existence.common.structure.constants.FormMode;
import dev.paradoxhorizon.existence.common.structure.constants.SourceScreen;
import dev.paradoxhorizon.existence.databinding.FragmentFoodItemBinding;
import dev.paradoxhorizon.existence.model.food.FoodModel;
import dev.paradoxhorizon.existence.repository.Food;
import dev.paradoxhorizon.existence.repository.FoodDatabase;
import dev.paradoxhorizon.existence.ui.food.FoodFragmentArgs;
import dev.paradoxhorizon.existence.ui.food.FoodFragmentDirections;

public class FoodFragment extends Fragment implements DeleteFoodDialog.DeleteFoodDialogListener {
    private static final String TAG = "FoodFragment";
    private int formMode;
    private int sourceScreen;
    private FragmentFoodItemBinding foodItemBinding;
    private FoodViewModel foodViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Hide the navigation bar
        BottomNavigationView navigationView = requireActivity().findViewById(R.id.nav_view);
        navigationView.setVisibility(View.INVISIBLE);
        // 1. Get the navigation arguments
        // Get the arguments passed by the calling fragment
        FoodFragmentArgs navArgs = FoodFragmentArgs.fromBundle(getArguments());
        formMode = navArgs.getFormmode();
        sourceScreen = navArgs.getSourcescreen();
        String data = navArgs.getFoodargs();
        // Transform the arguments to an object (model) that can be used by the views
        FoodModel foodModel = new Gson().fromJson(data, FoodModel.class);

        // 2. Create the ViewModel associated to the fragment
        // Instantiate the FoodViewModel that will hold the data for this fragment
        foodViewModel = new ViewModelProvider(this).get(FoodViewModel.class);
        // Save the newly transformed object to the viewmodel
        foodViewModel.setFoodModel(foodModel);

        // 3. Manipulate the fragment's views
        // Attach the xml layout to this fragment to be able to manipulate the views
        foodItemBinding = FragmentFoodItemBinding.inflate(inflater, container, false);
        // Create an observer to listen for data changes and update the views
        final Observer<FoodModel> foodModelObserver = getFoodModelObserver();
        // Attach the observer to the viewmodel
        foodViewModel.getFoodModel().observe(getViewLifecycleOwner(), foodModelObserver);

        return foodItemBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Set the form button text depending on the source screen
        switch (sourceScreen) {
            case 1:
                // From home screen
                foodItemBinding.fooditemButtonOkay.setText(R.string.thanks);
                break;
            case 3:
                // From dashboard screen, delete mode
                if (formMode == FormMode.DELETE) {
                    foodItemBinding.fooditemButtonOkay.setVisibility(View.INVISIBLE);
                }
                // From dashboard screen, read mode : no changes
            case 11:
                // From modify food screen
                foodItemBinding.fooditemButtonOkay.setText(R.string.done);
                break;
            default:
        }

        // Set the form button behavior depending on the source screen
        foodItemBinding.fooditemButtonOkay.setOnClickListener(v -> {
            switch (sourceScreen) {
                case 1:
                    // From home screen
                    Navigation.findNavController(requireView())
                            .navigate(FoodFragmentDirections.actionFoodFragmentToNavigationHome());
                    break;
                case 3:
                case 11:
                    // From both dashboard screen read mode and modify food screen
                    Navigation.findNavController(requireView())
                            .navigate(FoodFragmentDirections.actionFoodFragmentToNavigationDashboard());
                    break;
                default:
            }
        });

        // Automatically display delete confirmation dialog when on delete mode
        if (formMode == FormMode.DELETE) {
            DeleteFoodDialog dialog = new DeleteFoodDialog();
            dialog.setListener(this);
            dialog.show(getParentFragmentManager(), "DELETE_FOOD_DIALOG");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        foodItemBinding = null;
    }

    private @NonNull Observer<FoodModel> getFoodModelObserver() {
        //Create field to hold the views to be manipulated
        final TextView foodName = foodItemBinding.fooditemTextviewName;
        final TextView foodLocation = foodItemBinding.fooditemTextviewLocation;
        final TextView foodRating = foodItemBinding.fooditemTextviewRating;
        final RatingBar foodRatingStars = foodItemBinding.fooditemRatingbarRating;
        final TextView foodHealth = foodItemBinding.fooditemTextviewHealth;
        final RatingBar foodHealthStars = foodItemBinding.fooditemRatingbarHealth;
        final TextView foodPrice = foodItemBinding.fooditemTextviewPrice;

        // Create an observer to be attached to the viewmodel
        return model -> {
            //Called once the value of the related field on the viewmodel changes
            foodName.setText(model.getName());
            foodLocation.setText(model.getLocation());
            foodRating.setText(String.valueOf(model.getRating()));
            foodRatingStars.setRating((float) model.getRating());
            foodHealth.setText(String.valueOf(model.getHealth()));
            foodHealthStars.setRating((float) model.getHealth());
            foodPrice.setText(String.valueOf(model.getPrice()));
        };
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialogFragment) {
        foodViewModel.getFoodModel().observe(getViewLifecycleOwner(), foodModel -> {
            Gson gson = new Gson();
            String foodModelString = gson.toJson(foodModel);
            Food food = gson.fromJson(foodModelString, Food.class);
            FoodDatabase.getInstance(getContext()).foodDao().deleteFood(food);
            if (sourceScreen == SourceScreen.DASHBOARD) {
                Navigation.findNavController(requireView())
                        .navigate(FoodFragmentDirections.actionFoodFragmentToNavigationDashboard());
            }
        });
    }

    @Override
    public void onDialogCancel(DialogFragment dialogFragment) {
        if (sourceScreen == SourceScreen.DASHBOARD) {
            Navigation.findNavController(requireView())
                    .navigate(FoodFragmentDirections.actionFoodFragmentToNavigationDashboard());
        }
    }
}
