package dev.paradoxhorizon.existence.ui.food;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import dev.paradoxhorizon.existence.R;
import dev.paradoxhorizon.existence.databinding.FoodListLayoutBinding;
import dev.paradoxhorizon.existence.model.food.FoodModel;

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.ViewHolder> {
    private static final String TAG = "FoodAdapter";
    private final IFoodAdapter mListener; //used for callback
    private final List<FoodModel> foodModelList; // the list to be shown on the recyclerview

    // Constructor
    public FoodAdapter(List<FoodModel> foodModelList, IFoodAdapter mListener) {
        this.foodModelList = foodModelList;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public FoodAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FoodListLayoutBinding binding = FoodListLayoutBinding
                .inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        FoodModel foodModel = foodModelList.get(position);
        holder.binding.foodlistTextviewFoodName.setText(foodModel.getName());
        holder.binding.foodlistTextviewFoodLocation.setText(foodModel.getLocation());
        holder.binding.foodlistTextviewFoodRating.setText(String.valueOf(foodModel.getRating()));

        holder.binding.foodlistConstraintlayout1.setOnClickListener(v -> {
            if (position != RecyclerView.NO_POSITION) {
                Log.d(TAG, "Item on position " + position + " clicked.");
                mListener.processFoodItem(foodModel);
            }
        });

        holder.binding.foodlistImageviewOptions.setOnClickListener(v -> {
            Log.d(TAG, "Options clicked.");
            showPopupMenu(holder.binding.foodlistImageviewOptions, position);
        });
    }

    @Override
    public int getItemCount() {
        return foodModelList.size();
    }

    private void showPopupMenu(View view, int position) {
        PopupMenu popupMenu = new PopupMenu(view.getContext(), view, Gravity.END);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.list_options_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            Log.d(TAG, "Menu item clicked. " + item.getTitle());
            final String menuTitle = String.valueOf(item.getTitle());
            switch (menuTitle) {
                case "Edit":
                    mListener.editMenuClicked(foodModelList.get(position));
                    break;
                case "Delete":
                    mListener.deleteMenuClicked(foodModelList.get(position));
                    break;
                default:
            }
            return true;
        });
        popupMenu.show();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        FoodListLayoutBinding binding;

        public ViewHolder(FoodListLayoutBinding layoutBinding) {
            super(layoutBinding.getRoot());
            binding = layoutBinding;
        }
    }

    public interface IFoodAdapter {
        void processFoodItem(FoodModel foodModel);
        void editMenuClicked(FoodModel foodModel);
        void deleteMenuClicked(FoodModel foodModel);
    }
}
