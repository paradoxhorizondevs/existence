package dev.paradoxhorizon.existence.ui.food;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import dev.paradoxhorizon.existence.R;

public class SuccessFoodDialog extends DialogFragment {
    private static final String TAG = "SuccessFoodDialog";
    private SuccessFoodDialogListener listener;

    public interface SuccessFoodDialogListener {
        void onDialogPositiveClick(DialogFragment dialogFragment);
        void onDialogCancel(DialogFragment dialogFragment);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.success_food)
                .setPositiveButton(R.string.okay, (dialog, which) ->
                        listener.onDialogPositiveClick(SuccessFoodDialog.this));
        return builder.create();
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        listener.onDialogCancel(SuccessFoodDialog.this);
    }

    public void setListener(SuccessFoodDialogListener listener) {
        this.listener = listener;
    }
}
