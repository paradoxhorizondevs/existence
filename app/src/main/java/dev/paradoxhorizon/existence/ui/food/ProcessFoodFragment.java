package dev.paradoxhorizon.existence.ui.food;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;

import dev.paradoxhorizon.existence.R;
import dev.paradoxhorizon.existence.common.structure.constants.FormMode;
import dev.paradoxhorizon.existence.common.structure.constants.SourceScreen;
import dev.paradoxhorizon.existence.databinding.FragmentProcessFoodBinding;
import dev.paradoxhorizon.existence.model.food.FoodModel;
import dev.paradoxhorizon.existence.repository.Food;
import dev.paradoxhorizon.existence.repository.FoodDatabase;
import dev.paradoxhorizon.existence.ui.food.ProcessFoodFragmentArgs;
import dev.paradoxhorizon.existence.ui.food.ProcessFoodFragmentDirections;
import dev.paradoxhorizon.existence.ui.food.ProcessFoodFragmentDirections.ActionProcessFoodFragmentToFoodFragment;

public class ProcessFoodFragment extends Fragment implements SuccessFoodDialog.SuccessFoodDialogListener {
    private static final String TAG = "AddFoodFragment";
    private FragmentProcessFoodBinding processFoodBinding;
    private FoodModel foodModel;
    private int mode = FormMode.INSERT;
    private int sourceScreen;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Hide the navigation bar
        BottomNavigationView navigationView = requireActivity().findViewById(R.id.nav_view);
        navigationView.setVisibility(View.INVISIBLE);

        ProcessFoodFragmentArgs navArgs = ProcessFoodFragmentArgs.fromBundle(getArguments());
        mode = navArgs.getFormmode();
        sourceScreen = navArgs.getSourcescreen();

        if (mode == FormMode.UPDATE) {
            String data = navArgs.getFoodargs();
            processFoodJson(data);
        }

        processFoodBinding = FragmentProcessFoodBinding.inflate(inflater, container, false);
        return processFoodBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // If the screen is in edit mode, fill out the fields
        if (mode == FormMode.UPDATE) {
            Log.d(TAG, "Editing food entry number " + foodModel.getUid() + ".");
            processFoodBinding.processfoodEdittextName.setText(foodModel.getName());
            processFoodBinding.processfoodEdittextLocation.setText(foodModel.getLocation());
            processFoodBinding.processfoodRatingbarRating.setRating((float) foodModel.getRating());
            processFoodBinding.processfoodEdittextPrice.setText(String.valueOf(foodModel.getPrice()));
            processFoodBinding.processfoodRatingbarHealth.setRating((float) foodModel.getHealth());
        }

        processFoodBinding.processfoodButtonAdd.setOnClickListener(v -> {
            if (!areDetailsFilled()) {
                EmptyFoodDialog emptyFoodDialog = new EmptyFoodDialog();
                emptyFoodDialog.show(getParentFragmentManager(), "INCOMPLETE_DETAILS");
            } else {
                if (mode == FormMode.INSERT) {
                    Food food = Food.builder()
                            .name(processFoodBinding.processfoodEdittextName.getText().toString())
                            .location(processFoodBinding.processfoodEdittextLocation.getText().toString())
                            .rating(processFoodBinding.processfoodRatingbarRating.getRating())
                            .price(Double.parseDouble(processFoodBinding.processfoodEdittextPrice.getText().toString()))
                            .health(processFoodBinding.processfoodRatingbarHealth.getRating())
                            .build();
                    FoodDatabase.getInstance(requireActivity()).foodDao().insertFood(food);
                    processNewFood(food);
                    SuccessFoodDialog successFoodDialog = new SuccessFoodDialog();
                    successFoodDialog.setListener(this);
                    successFoodDialog.show(getParentFragmentManager(), "ADD FOOD SUCCESS");
                } else if (mode == FormMode.UPDATE) {
                    Food food = FoodDatabase.getInstance(requireActivity()).foodDao().getFoodByUid(foodModel.getUid()).get();
                    food.setName(processFoodBinding.processfoodEdittextName.getText().toString());
                    food.setLocation(processFoodBinding.processfoodEdittextLocation.getText().toString());
                    food.setRating(processFoodBinding.processfoodRatingbarRating.getRating());
                    food.setPrice(Double.parseDouble(processFoodBinding.processfoodEdittextPrice.getText().toString()));
                    food.setHealth(processFoodBinding.processfoodRatingbarHealth.getRating());
                    FoodDatabase.getInstance(requireActivity()).foodDao().updateFood(food);
                    processNewFood(food);
                    SuccessFoodDialog successFoodDialog = new SuccessFoodDialog();
                    successFoodDialog.setListener(this);
                    successFoodDialog.show(getParentFragmentManager(), "UPDATE FOOD SUCCESS");
                }

            }
        });
    }

    private boolean areDetailsFilled() {
        return !processFoodBinding.processfoodEdittextName.getText().toString().isBlank() &&
                !processFoodBinding.processfoodEdittextLocation.getText().toString().isBlank() &&
                !processFoodBinding.processfoodEdittextPrice.getText().toString().isBlank();
    }

    private void processNewFood(Food food) {
        Gson gson = new Gson();
        String foodDbString = gson.toJson(food);
        foodModel = gson.fromJson(foodDbString, FoodModel.class);
    }

    private void processFoodJson(String foodJson) {
        Gson gson = new Gson();
        foodModel = gson.fromJson(foodJson, FoodModel.class);
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialogFragment) {
        Gson gson = new Gson();
        String foodJson = gson.toJson(foodModel);
        ProcessFoodFragmentDirections.ActionProcessFoodFragmentToFoodFragment addFoodAction =
                ProcessFoodFragmentDirections.actionProcessFoodFragmentToFoodFragment();
        addFoodAction.setSourcescreen(SourceScreen.MODIFY_FOOD);
        addFoodAction.setFoodargs(foodJson);
        Navigation.findNavController(requireView()).navigate(addFoodAction);
    }

    @Override
    public void onDialogCancel(DialogFragment dialogFragment) {
        Gson gson = new Gson();
        String foodJson = gson.toJson(foodModel);
        ProcessFoodFragmentDirections.ActionProcessFoodFragmentToFoodFragment addFoodAction =
                ProcessFoodFragmentDirections.actionProcessFoodFragmentToFoodFragment();
        addFoodAction.setSourcescreen(SourceScreen.MODIFY_FOOD);
        addFoodAction.setFoodargs(foodJson);
        Navigation.findNavController(requireView()).navigate(addFoodAction);
    }
}
