package dev.paradoxhorizon.existence.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.sqlite.db.SimpleSQLiteQuery;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import dev.paradoxhorizon.existence.R;
import dev.paradoxhorizon.existence.common.structure.constants.SourceScreen;
import dev.paradoxhorizon.existence.common.utils.NumberUtil;
import dev.paradoxhorizon.existence.databinding.FragmentHomeBinding;
import dev.paradoxhorizon.existence.model.filter.FilterFoodModel;
import dev.paradoxhorizon.existence.model.food.FoodModel;

import dev.paradoxhorizon.existence.repository.Food;
import dev.paradoxhorizon.existence.repository.FoodDatabase;

import dev.paradoxhorizon.existence.ui.food.FilterFoodDialog;
import dev.paradoxhorizon.existence.ui.food.NoFoodDialog;
import dev.paradoxhorizon.existence.ui.home.HomeFragmentDirections;
import dev.paradoxhorizon.existence.ui.home.HomeFragmentDirections.ActionNavigationHomeToFoodFragment;

public class HomeFragment extends Fragment implements FilterFoodDialog.FilterFoodDialogListener {
    private static final String TAG = "HomeFragment";
    private FragmentHomeBinding homeBinding;
    private FilterFoodModel filters;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Ensure that the navigation bar is shown
        BottomNavigationView navigationView = requireActivity().findViewById(R.id.nav_view);
        navigationView.setVisibility(View.VISIBLE);
        homeBinding = FragmentHomeBinding.inflate(inflater, container, false);
        return homeBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        homeBinding.homeButtonFetchFood.setOnClickListener(v -> {
            int foodCount = FoodDatabase.getInstance(requireActivity()).foodDao().getFoodCount();
            if (foodCount > 0) {
                if (homeBinding.homeCheckboxFilter.isChecked()) {
                    SimpleSQLiteQuery filterQuery = buildFilters();
                    List<Food> filteredList = FoodDatabase.getInstance(requireActivity())
                            .foodDao()
                            .getFoodByFilters(filterQuery);
                    if (filteredList.isEmpty()) {
                        NoFoodDialog dialog = new NoFoodDialog();
                        dialog.setMessageId(R.string.no_food_dialog);
                        dialog.show(getParentFragmentManager(), "NO_FOOD");
                        homeBinding.homeCheckboxFilter.setChecked(false);
                    } else if (filteredList.size() == 1) {
                        Gson gson = new Gson();
                        String foodString = gson.toJson(filteredList.get(0));
                        FoodModel model = gson.fromJson(foodString, FoodModel.class);
                        processFoodItem(model);
                    } else {
                        int random = NumberUtil.generateRandomNumber(0, filteredList.size() - 1);
                        Food food = filteredList.get(random);
                        Gson gson = new Gson();
                        String foodDbString = gson.toJson(food);
                        FoodModel model = gson.fromJson(foodDbString, FoodModel.class);
                        processFoodItem(model);
                    }
                } else {
                    boolean foundFood = false;
                    int counter = 1;
                    do {
                        Log.d(TAG, "Do loop triggered " + counter + " times.");
                        counter++;
                        int min = FoodDatabase.getInstance(requireActivity()).foodDao().getMinFoodIndex();
                        int max = FoodDatabase.getInstance(requireActivity()).foodDao().getMaxFoodIndex();
                        int random = NumberUtil.generateRandomNumber(min, max);
                        Food food = FoodDatabase.getInstance(requireActivity())
                                .foodDao()
                                .getFoodByUid(random)
                                .orElse(null);
                        if (food != null) {
                            foundFood = true;
                            Gson gson = new Gson();
                            String foodDbString = gson.toJson(food);
                            FoodModel model = gson.fromJson(foodDbString, FoodModel.class);
                            processFoodItem(model);
                        }
                    }
                    while (!foundFood);
                }
            } else {
                NoFoodDialog dialog = new NoFoodDialog();
                dialog.setMessageId(R.string.empty_food);
                dialog.show(getParentFragmentManager(), "NO_FOOD");

                if (homeBinding.homeCheckboxFilter.isChecked()) homeBinding.homeCheckboxFilter.setChecked(false);
            }
        });

        homeBinding.homeCheckboxFilter.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                FilterFoodDialog filterFoodDialog = new FilterFoodDialog();
                filterFoodDialog.setListener(HomeFragment.this);
                filterFoodDialog.show(getParentFragmentManager(), "FILTER_FOOD");
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        homeBinding = null;
    }

    public void processFoodItem(FoodModel foodModel) {
        // Create gson object to convert object to json and vice versa
        Gson gson = new Gson();
        String foodJson = gson.toJson(foodModel);
        // Build the intent to navigate to a new fragment
        HomeFragmentDirections.ActionNavigationHomeToFoodFragment homeAction =
                HomeFragmentDirections.actionNavigationHomeToFoodFragment();
        // Set the arguments to be sent upon navigation
        homeAction.setSourcescreen(SourceScreen.HOME);
        homeAction.setFoodargs(foodJson);
        // Execute the actual navigation process
        Navigation.findNavController(requireView()).navigate(homeAction);
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialogFragment, FilterFoodModel filter) {
        this.filters = filter;
        if (buildFilters() == null) {
            NoFoodDialog dialog = new NoFoodDialog();
            dialog.setMessageId(R.string.empty_fields);
            dialog.show(getParentFragmentManager(), "NO_FOOD");
            homeBinding.homeCheckboxFilter.setChecked(false);
        }
    }

    @Override
    public void onDialogCancel(DialogFragment dialogFragment) {
        homeBinding.homeCheckboxFilter.setChecked(false);
    }

    private SimpleSQLiteQuery buildFilters() {
        String name = filters.getName();
        double minRating = filters.getMinRating();
        double maxRating = filters.getMaxRating();
        double minHealth = filters.getMinHealth();
        double maxHealth = filters.getMaxHealth();
        double priceMin = filters.getMinPrice();
        double priceMax = filters.getMaxPrice();

        String query = "";
        List<Object> queryArgs = new ArrayList<>();
        boolean hasConditions = false;

        query += "SELECT * FROM foods";

        if (!name.isEmpty()) {
            query += " WHERE";
            query += " name LIKE ?";
            queryArgs.add(name.length() > 1 ? "%" + name + "%" : name + "%");
            hasConditions = true;
        }

        if (minRating > 0) {
            query += hasConditions ? " AND" : " WHERE";
            query += " rating >= ?";
            queryArgs.add(minRating);
            hasConditions = true;
        }

        if (maxRating > 0) {
            query += hasConditions ? " AND" : " WHERE";
            query += " rating <= ?";
            queryArgs.add(maxRating);
            hasConditions = true;
        }

        if (minHealth > 0) {
            query += hasConditions ? " AND" : " WHERE";
            query += " health >= ?";
            queryArgs.add(minHealth);
            hasConditions = true;
        }

        if (maxHealth > 0) {
            query += hasConditions ? " AND" : " WHERE";
            query += " health <= ?";
            queryArgs.add(maxHealth);
            hasConditions = true;
        }

        if (priceMin > 0) {
            query += hasConditions ? " AND" : " WHERE";
            query += " price >= ?";
            queryArgs.add(priceMin);
            hasConditions = true;
        }

        if (priceMax > 0) {
            query += hasConditions ? " AND" : " WHERE";
            query += " price <= ?";
            queryArgs.add(priceMax);
        }

        query += ";";

        if (!hasConditions) return null;

        return new SimpleSQLiteQuery(query, queryArgs.toArray());
    }
}
