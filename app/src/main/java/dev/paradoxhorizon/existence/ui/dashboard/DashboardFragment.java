package dev.paradoxhorizon.existence.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import dev.paradoxhorizon.existence.R;
import dev.paradoxhorizon.existence.common.structure.constants.FormMode;
import dev.paradoxhorizon.existence.common.structure.constants.SourceScreen;
import dev.paradoxhorizon.existence.databinding.FragmentDashboardBinding;
import dev.paradoxhorizon.existence.model.food.FoodModel;
import dev.paradoxhorizon.existence.repository.Food;
import dev.paradoxhorizon.existence.repository.FoodDatabase;
import dev.paradoxhorizon.existence.ui.dashboard.DashboardFragmentDirections;
import dev.paradoxhorizon.existence.ui.dashboard.DashboardFragmentDirections.ActionNavigationDashboardToFoodFragment;
import dev.paradoxhorizon.existence.ui.food.FoodAdapter;

public class DashboardFragment extends Fragment implements FoodAdapter.IFoodAdapter {
    private static final String TAG = "DashboardFragment";
    private FragmentDashboardBinding dashboardBinding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        DashboardViewModel dashboardViewModel = new ViewModelProvider(this).get(DashboardViewModel.class);
        // Ensure that the navigation bar is shown
        BottomNavigationView navigationView = requireActivity().findViewById(R.id.nav_view);
        navigationView.setVisibility(View.VISIBLE);
        dashboardBinding = FragmentDashboardBinding.inflate(inflater, container, false);
        View root = dashboardBinding.getRoot();
        int foodCount = FoodDatabase.getInstance(requireActivity()).foodDao().getFoodCount();
        if (foodCount > 0) {
            RecyclerView foodListView = dashboardBinding.dashboardListFoods;
            List<Food> foodDbList = FoodDatabase.getInstance(requireActivity()).foodDao().getAllFood();
            List<FoodModel> foodModelList = new ArrayList<>();
            for (Food food: foodDbList) {
                Gson gson = new Gson();
                String foodString = gson.toJson(food);
                FoodModel foodModel = gson.fromJson(foodString, FoodModel.class);
                foodModelList.add(foodModel);
            }
            FoodAdapter foodAdapter = new FoodAdapter(foodModelList, DashboardFragment.this);
            foodListView.setAdapter(foodAdapter);
            foodListView.setLayoutManager(new LinearLayoutManager(requireActivity()));
        } else {
            dashboardBinding.dashboardLabelEmpty.setVisibility(View.VISIBLE);
        }

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dashboardBinding.dashboardFabAdd.setOnClickListener(v -> {
            DashboardFragmentDirections.ActionNavigationDashboardToProcessFoodFragment action =
                    DashboardFragmentDirections.actionNavigationDashboardToProcessFoodFragment();
            action.setSourcescreen(SourceScreen.DASHBOARD);
            action.setFormmode(FormMode.INSERT);
            Navigation.findNavController(requireView()).navigate(action);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dashboardBinding = null;
    }

    @Override
    public void processFoodItem(FoodModel foodModel) {
        Gson gson = new Gson();
        String foodJson = gson.toJson(foodModel);
        DashboardFragmentDirections.ActionNavigationDashboardToFoodFragment action =
                DashboardFragmentDirections.actionNavigationDashboardToFoodFragment();
        action.setSourcescreen(SourceScreen.DASHBOARD);
        action.setFoodargs(foodJson);
        action.setFormmode(FormMode.READ);
        Navigation.findNavController(requireView()).navigate(action);
    }

    @Override
    public void editMenuClicked(FoodModel foodModel) {
        Gson gson = new Gson();
        String foodJson = gson.toJson(foodModel);
        DashboardFragmentDirections.ActionNavigationDashboardToProcessFoodFragment action =
                DashboardFragmentDirections.actionNavigationDashboardToProcessFoodFragment();
        action.setSourcescreen(SourceScreen.DASHBOARD);
        action.setFoodargs(foodJson);
        action.setFormmode(FormMode.UPDATE);
        Navigation.findNavController(requireView()).navigate(action);
    }

    @Override
    public void deleteMenuClicked(FoodModel foodModel) {
        Gson gson = new Gson();
        String foodJson = gson.toJson(foodModel);
        DashboardFragmentDirections.ActionNavigationDashboardToFoodFragment action =
                DashboardFragmentDirections.actionNavigationDashboardToFoodFragment();
        action.setSourcescreen(SourceScreen.DASHBOARD);
        action.setFoodargs(foodJson);
        action.setFormmode(FormMode.DELETE);
        Navigation.findNavController(requireView()).navigate(action);
    }
}
