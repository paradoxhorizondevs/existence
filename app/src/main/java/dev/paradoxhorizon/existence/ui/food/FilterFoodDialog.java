package dev.paradoxhorizon.existence.ui.food;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import dev.paradoxhorizon.existence.R;
import dev.paradoxhorizon.existence.common.utils.NumberUtil;
import dev.paradoxhorizon.existence.databinding.DialogFoodFiltersBinding;
import dev.paradoxhorizon.existence.model.filter.FilterFoodModel;

public class FilterFoodDialog extends DialogFragment {
    private static final String TAG = "FilterFoodDialog";
    private FilterFoodDialogListener listener;
    private FilterFoodModel mFilter;
    private DialogFoodFiltersBinding foodFiltersBinding;

    public interface FilterFoodDialogListener {
        void onDialogPositiveClick(DialogFragment dialogFragment, FilterFoodModel filter);
        void onDialogCancel(DialogFragment dialogFragment);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        foodFiltersBinding = DialogFoodFiltersBinding.inflate(getLayoutInflater());
        builder.setView(foodFiltersBinding.getRoot())
                .setPositiveButton(R.string.okay, (dialog, which) -> {
                    mFilter = FilterFoodModel.builder()
                            .name(foodFiltersBinding.foodFilterEdittextName.getText().toString())
                            .minRating(NumberUtil.parseDoubleSafely(foodFiltersBinding.foodFilterEdittextRatingMin.getText().toString()))
                            .maxRating(NumberUtil.parseDoubleSafely(foodFiltersBinding.foodFilterEdittextRatingMax.getText().toString()))
                            .minHealth(NumberUtil.parseDoubleSafely(foodFiltersBinding.foodFilterEdittextHealthMin.getText().toString()))
                            .maxHealth(NumberUtil.parseDoubleSafely(foodFiltersBinding.foodFilterEdittextHealthMax.getText().toString()))
                            .minPrice(NumberUtil.parseDoubleSafely(foodFiltersBinding.foodFilterEdittextPriceMin.getText().toString()))
                            .maxPrice(NumberUtil.parseDoubleSafely(foodFiltersBinding.foodFilterEdittextPriceMin.getText().toString()))
                            .build();
                    listener.onDialogPositiveClick(FilterFoodDialog.this, mFilter);
                })
                .setNegativeButton(R.string.cancel, (dialog, which) ->
                        listener.onDialogCancel(FilterFoodDialog.this));
        return builder.create();
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        listener.onDialogCancel(FilterFoodDialog.this);
    }

    public void setListener(FilterFoodDialogListener listener) {
        this.listener = listener;
    }
}
