package dev.paradoxhorizon.existence.ui.food;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import dev.paradoxhorizon.existence.R;

public class DeleteFoodDialog extends DialogFragment {
    private static final String TAG = "DeleteFoodDialog";
    private DeleteFoodDialogListener listener;

    public interface DeleteFoodDialogListener {
        void onDialogPositiveClick(DialogFragment dialogFragment);
        void onDialogCancel(DialogFragment dialogFragment);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.question_delete_food)
                .setPositiveButton(R.string.okay, (dialog, which) ->
                        listener.onDialogPositiveClick(DeleteFoodDialog.this))
                .setNegativeButton(R.string.cancel, (dialog, which) ->
                        listener.onDialogCancel(DeleteFoodDialog.this));
        return builder.create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setGravity(Gravity.BOTTOM | Gravity.CENTER_VERTICAL);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        listener.onDialogCancel(DeleteFoodDialog.this);
    }

    public void setListener(DeleteFoodDialogListener listener) {
        this.listener = listener;
    }
}
