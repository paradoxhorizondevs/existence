package dev.paradoxhorizon.existence.ui.food;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import dev.paradoxhorizon.existence.R;

public class EmptyFoodDialog extends DialogFragment {
    private static final String TAG = "EmptyFoodDialog";

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.empty_fields)
                .setPositiveButton(R.string.okay, (dialog, which) -> {
                });
        return builder.create();
    }
}
