package dev.paradoxhorizon.existence.repository;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
@Entity(tableName = "foods")
public class Food {
    @PrimaryKey(autoGenerate = true)
    private int uid;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "health")
    private double health;
    @ColumnInfo(name = "price")
    private double price;
    @ColumnInfo(name = "rating")
    private double rating;
    @ColumnInfo(name = "location")
    private String location;
}