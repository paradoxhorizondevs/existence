package dev.paradoxhorizon.existence.repository;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.room.Update;
import androidx.sqlite.db.SupportSQLiteQuery;

import java.util.List;
import java.util.Optional;

@Dao
public interface FoodDao {
    @Query("SELECT * FROM foods")
    List<Food> getAllFood();
    // TODO: This should be a LiveData and not Optional.
    @Query("SELECT * FROM foods WHERE uid = :UID")
    Optional<Food> getFoodByUid(int UID);
    @Query("SELECT COUNT(uid) FROM foods")
    int getFoodCount();
    @Query("SELECT uid FROM foods ORDER BY uid DESC LIMIT 1")
    int getMaxFoodIndex();
    @Query("SELECT uid FROM foods ORDER BY uid ASC LIMIT 1")
    int getMinFoodIndex();

    @Insert
    void insertFood(Food food);

    @Update
    void updateFood(Food food);

    @Delete
    void deleteFood(Food food);

    @RawQuery
    List<Food> getFoodByFilters(SupportSQLiteQuery query);
}
